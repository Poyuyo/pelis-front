import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { MovieDetail } from 'src/interfaces/MovieDetail';
import { MoviesSeriesApi, MovieSerieUser } from 'src/interfaces/MoviesSeriesApi';
import { SerieDetail } from 'src/interfaces/SerieDetail';
import { AngularFirestore } from '@angular/fire/compat/firestore'

@Injectable({
  providedIn: 'root'
})
export class MoviesService {

  private base_url: string = 'https://api.themoviedb.org/3';

  private api_key: string = 'a49dee80945e6c2f84d6b86e2b16a0bd';

  private lang: string = 'es';

  constructor(private _http: HttpClient, private firestore: AngularFirestore) {


  }

  getTrending(): Observable<MoviesSeriesApi[]> {
    const params = { params: new HttpParams().set('api_key', this.api_key).set('language', this.lang) }
    let response = this._http.get<MoviesSeriesApi[]>(this.base_url + '/trending/all/week', params)

    return response;

  }

  getMovies(): Observable<MoviesSeriesApi[]> {
    const params = { params: new HttpParams().set('api_key', this.api_key).set('language', this.lang) }
    let response = this._http.get<MoviesSeriesApi[]>(this.base_url + '/movie/popular', params)

    return response;
  }

  getSeries(): Observable<MoviesSeriesApi[]> {
    const params = { params: new HttpParams().set('api_key', this.api_key).set('language', this.lang) }
    let response = this._http.get<MoviesSeriesApi[]>(this.base_url + '/tv/popular', params)

    return response;
  }

  getMovie(id: number): Observable<MovieDetail> {
    const params = { params: new HttpParams().set('api_key', this.api_key).set('language', this.lang) }
    let response = this._http.get<MovieDetail>(this.base_url + '/movie/' + id, params)

    return response;
  }

  getSerie(id: number): Observable<SerieDetail> {
    const params = { params: new HttpParams().set('api_key', this.api_key).set('language', this.lang) }
    let response = this._http.get<SerieDetail>(this.base_url + '/tv/' + id, params)

    return response;
  }

  //----------------------------------Firebase-------------------------------------

  addItem(userId: MovieSerieUser, item: MoviesSeriesApi): Promise<any> {
    if (item.media_type == 'movie')
      return this.firestore.collection('usuarios').doc(`${userId}`).collection('movies').add(item)
    else
      return this.firestore.collection('usuarios').doc(`${userId}`).collection('tv').add(item)
  }

  getMyList(userId: MovieSerieUser, type: string): Observable<any> {
    if (type == 'movie')
      return this.firestore.collection('usuarios').doc(`${userId}`).collection('movies').snapshotChanges();
    else
      return this.firestore.collection('usuarios').doc(`${userId}`).collection('tv').snapshotChanges();
  }

  deleteItem(userId: string, id: string, type: string): Promise<any> {
    console.log("service type ", type)
    console.log("service id ", id)

    console.log("service uid ", userId)

    if (type == 'movie')
      return this.firestore.collection(`usuarios/${userId}/movies`).doc(id).delete();
    else
      return this.firestore.collection(`usuarios/${userId}/tv`).doc(id).delete();
  }

  async countItems(userId: string, type: string) {
    if (type == 'movie') {
      const count = this.firestore.collection('usuarios').doc(`${userId}`).collection('movies').get();
      let cont = 0;
      return count.forEach(() => {
        cont++;
      });

    }
    else {
      const count = this.firestore.collection('usuarios').doc(`${userId}`).collection('tv').snapshotChanges();
      return count;
    }
  }
}
