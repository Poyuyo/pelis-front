import { Injectable } from '@angular/core';
import firebase from 'firebase/compat/app';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { createUserWithEmailAndPassword, getAuth, onAuthStateChanged } from 'firebase/auth'
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  // user: any;

  constructor(public afAuth: AngularFireAuth, private router: Router) { }

  async login(email: string, password: string) {

    return await this.afAuth.signInWithEmailAndPassword(email, password)
      .then((result) => {
        console.log("Login email y pass exitoso")
        return result;
      })
      .catch((err) => {
        console.log("Error en login", err)
        return null;
      })
  }

  async loginWithGoogle() {
    return await this.afAuth.signInWithPopup(new firebase.auth.GoogleAuthProvider())
      .then((result) => {
        // this.user = result.user;
        // this.router.navigate(['dashboard'])
        return result;
      })
      .catch((err) => {
        console.log("Error en login con google ", err)
        return null;
      })
  }

  // async getUserLogged() {
  //   const auth = getAuth();
  //   const user = auth.currentUser;
  //   if (user !== null) {
  //     this.user.displayName = user.displayName ? user.displayName : '';
  //     this.user.email = user.email ? user.email : '';
  //   } else {
  //     return null;
  //   }

  //   return this.user;

  // }

  async signup(email: string, password: string) {
    return this.afAuth.createUserWithEmailAndPassword(email, password)
      .then((userCredential) => {
        // this.router.navigate(['dashboard'])
        return userCredential;
      })
      .catch((error) => {
        console.log("Error en register: ", error)
        return null;
      });
  }

  logout() {
    this.afAuth.signOut();
  }

  // validateIfLogged(): Boolean {

  //   const auth = getAuth();
  //   onAuthStateChanged(auth, (user) => {
  //     if (user) {
  //       return true;
  //     } else {
  //       return false;
  //     }
  //   })

  //   return false;
  // }

}
