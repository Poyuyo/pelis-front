import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MoviesService } from 'src/app/services/movies.service';
import { Genre, MovieDetail } from 'src/interfaces/MovieDetail';
import { SerieDetail } from 'src/interfaces/SerieDetail';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {
  id: number = 0;
  media_type: string = '';
  genres: string="";
  movieDetail!: MovieDetail;
  serieDetail!: SerieDetail;
  urlPoster: string = "https://image.tmdb.org/t/p/w500";
  urlPortada: string = "https://image.tmdb.org/t/p/w780";


  constructor(private route: ActivatedRoute, private _moviesService: MoviesService) { }

  ngOnInit(): void {
    this.getDetails();
  }

  getDetails() {

    this.id = this.route.snapshot.queryParams['id'];
    this.media_type = this.route.snapshot.queryParams['media_type'];

    if (this.media_type == 'tv') {
      this._moviesService.getSerie(this.id).subscribe({
        next: (data: SerieDetail) => {
          this.serieDetail = data;
          for(let genre of this.serieDetail.genres){
            this.genres += (genre.name +", ")
          }
          this.genres= this.genres.substring(0, this.genres.length - 2);
          console.log(data)
        },
        error: (err) => {
          console.log(err)
        }
      })
    } else {
      this._moviesService.getMovie(this.id).subscribe({
        next: (data: MovieDetail) => {
          this.movieDetail = data;
          for(let genre of this.movieDetail.genres){
            this.genres += (genre.name +", ")
          }
          this.genres= this.genres.substring(0, this.genres.length - 2);
        },
        error: (err) => {
          console.log(err)
        }
      })
    }

  }



}
