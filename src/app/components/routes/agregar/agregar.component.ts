import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { MoviesService } from 'src/app/services/movies.service';
import { MoviesSeriesApi } from 'src/interfaces/MoviesSeriesApi';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.component.html',
  styleUrls: ['./agregar.component.css']
})
export class AgregarComponent implements OnInit {

  constructor(private _moviesService: MoviesService, private _authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.getTrending()
  }

  toSearch: string = "";
  media_type: string = ""

  movies_series: MoviesSeriesApi[] = [];
  movies_series_show: MoviesSeriesApi[] = [];

  movies_series_search: MoviesSeriesApi[] = [];


  async getTrending() {
    
    this._moviesService.getTrending().subscribe({
      next: (data: any) => {
        this.setLists(data)
      },
      error: (err) => {
        console.log(err)
      }
    })
  }

  setLists(data: any) {
    this.movies_series = data.results;
    this.movies_series_show = data.results;
  }

  searchMovieOrSerie(e: string) {

    this.movies_series_search = [];
    this.toSearch = e.toUpperCase();

    this.movies_series_search = this.movies_series.filter(movie_serie => (movie_serie.title + "").concat(movie_serie.name + "").toUpperCase().includes(this.toSearch))

    if (e !== '') {
      this.movies_series_show = this.movies_series_search
    } else {
      this.movies_series_show = this.movies_series;
    }
  }

}
