import { Component, Input, OnInit } from '@angular/core';
import { MoviesSeriesApi } from 'src/interfaces/MoviesSeriesApi';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { MoviesService } from 'src/app/services/movies.service';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  @Input()
  movie_serie!: MoviesSeriesApi;

  url: string = "https://image.tmdb.org/t/p/w500";

  @Input() media_type: string = "";
  @Input() routeCall: string = "";

  user: any;
  mostrar: boolean = true;

  myMovies: MoviesSeriesApi[] = [];
  mySeries: MoviesSeriesApi[] = [];

  constructor(private router: Router, private _authService: AuthService, private _movieService: MoviesService) { }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('usuario') || '{}')

    let type = this.movie_serie.media_type

    this._movieService.getMyList(this.user.uid, type).subscribe(
      response => {
        this.myMovies = [];
        response.forEach((element: any) => {
          this.myMovies.push({
            globalId: element.payload.doc.id,
            ...element.payload.doc.data()
          });

        });

        if (this.myMovies.find(element => element.id == this.movie_serie.id)) {
          this.mostrar = false;
        } else
          this.mostrar = true;

      })

  }


  gotToDetails() {

    if (this.movie_serie.media_type != null) {
      if (this.movie_serie.media_type == 'tv') {
        this.router.navigate(['series/details'], { queryParams: { id: this.movie_serie.id, media_type: this.movie_serie.media_type } });
      }
      else {
        this.router.navigate(['movies/details'], { queryParams: { id: this.movie_serie.id, media_type: this.movie_serie.media_type } });
      }
    } else {
      if (this.media_type == 'tv') {
        this.router.navigate(['series/details'], { queryParams: { id: this.movie_serie.id, media_type: this.media_type } });
      }
      else {
        this.router.navigate(['movies/details'], { queryParams: { id: this.movie_serie.id, media_type: this.media_type } });
      }
    }

  }

  add() {
    this._movieService.addItem(this.user.uid, this.movie_serie);
  }

  remove() {
    this._movieService.deleteItem(this.user.uid, this.movie_serie.globalId || '', this.movie_serie.media_type)
      .then(() => {
        console.log("Elemento eliminado")
      })
      .catch((err) => {
        console.log("Ocurrio un error al eliminar: ", err)
      })
  }


}
